<html lang="en">
<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>WebGL Cube Slow</title>
  <meta name="description" content="The HTML5 Herald">
  <meta name="author" content="Gregory Hammond">

  <!-- Tells Google not to provide a translation for this page -->
  <meta name="google" content="notranslate">

  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->

  <style>
    body 
    {
      overflow: hidden;
    }
  </style>

</head>

<body>

  <script src="js/three.js"></script>

  <script>
    var scene, camera, renderer;
    var geometry, material, mesh;

    init();
    animate();

    function init() 
    {

	  scene = new THREE.Scene();

	  camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 1, 10000 );
	  camera.position.z = 200;

	  geometry = new THREE.BoxGeometry( 400, 400, 400 );
	  material = new THREE.MeshBasicMaterial( { color: 0xff0000, wireframe: true } );

	  mesh = new THREE.Mesh( geometry, material );
	  scene.add( mesh );

	  renderer = new THREE.WebGLRenderer();
	  renderer.setSize( window.innerWidth, window.innerHeight );

	  document.body.appendChild( renderer.domElement );

    }

    function animate() 
    {

	  requestAnimationFrame( animate );

	  mesh.rotation.x += 0.001;
	  mesh.rotation.y += 0.002;

	  renderer.render( scene, camera );

    }
    </script>

</body>
</html>