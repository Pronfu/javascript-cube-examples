# javascript-cube-examples
Cube examples using three.js. License is MIT.

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Created a slow, medium and fast speed cubes to help you understand three.js, Javascript, and PHP. The site has html5shiv so IE (Internet Explorer) users can see the cubes.

## License

[MIT](https://opensource.org/licenses/MIT)
